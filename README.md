# Kube

Docker image with Kubernetes tools for deploying applications

## Tools

### kubectl

The current version at the time the docker image was created

see also https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-kubectl-binary-with-curl-on-linux

```bash
docker run --rm -it registry.gitlab.com/epicdocker/kube:latest kubectl version --client
```

### Helm 

Helm version 3.6.3 downloaded from get.helm.sh

```bash
docker run --rm -it registry.gitlab.com/epicdocker/kube:latest helm version
```

### Docker (in Docker)

The image is based on Docker in Docker version 20.10.8

```
docker run --rm -it registry.gitlab.com/epicdocker/kube:latest docker --version
```

### yq

The current version at the time the docker image was created

see also https://github.com/mikefarah/yq/releases

```bash
docker run --rm -it registry.gitlab.com/epicdocker/kube:latest yq --version
```

### Other 

The currently available version from the Alpine packages

see also https://pkgs.alpinelinux.org/packages

* curl 
* git
* jq
* openssl

```bash
docker run --rm -it registry.gitlab.com/epicdocker/kube:latest git --version
docker run --rm -it registry.gitlab.com/epicdocker/kube:latest openssl version
```

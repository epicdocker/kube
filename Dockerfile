FROM docker:20.10.8-dind
#### Based on alpine 3.14.2

LABEL image.name="epicsoft_kube" \
      image.description="Docker image with Kubernetes tools for deploying applications" \
      maintainer="epicsoft.de" \
      maintainer.name="Alexander Schwarz <schwarz@epicsoft.de>" \
      maintainer.copyright="Copyright 2020-2021 epicsoft.de / Alexander Schwarz" \
      license="MIT" \
      kube_version="1.6.0"

RUN apk --no-cache add curl \
                       git \
                       jq \
                       openssl \
 && rm -f /var/cache/apk/*

#### In the next alpine version kubectl can be installed via apk
#### See https://pkgs.alpinelinux.org/packages?name=kubectl&branch=edge
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl \
 && mv kubectl /bin/kubectl \
 && chmod +x /bin/kubectl

#### In the next alpine version helm can be installed via apk
RUN curl -L https://get.helm.sh/helm-v3.6.3-linux-amd64.tar.gz | tar xvz \
 && mv linux-amd64/helm /bin/helm \
 && chmod +x /bin/helm \
 && rm -rf linux-amd64

#### https://github.com/mikefarah/yq
#### https://github.com/mikefarah/yq/releases
RUN curl -LO $(curl -s https://api.github.com/repos/mikefarah/yq/releases/latest \
    | grep browser_download_url \
    | grep -v "tar.gz" \
    | grep yq_linux_amd64 \
    | cut -d '"' -f 4) \
 && mv yq_linux_amd64 /bin/yq \
 && chmod +x /bin/yq

CMD echo "visit the project at https://gitlab.com/epicdocker/kube"
